package com.example.quantumapp.data

import androidx.annotation.Keep

@Keep
data class QuantumSplashResponse(val url : String)