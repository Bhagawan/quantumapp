package com.example.quantumapp.data

import java.util.*

data class Investment(val date: Date, val currency: String, val amount: Int)
