package com.example.quantumapp.data.static

const val UrlSplash = "QuantumApp/splash.php"
const val UrlLogo = "http://195.201.125.8/QuantumApp/assets/logo.png"
const val UrlBack = "http://195.201.125.8/QuantumApp/assets/back.jpg"
const val UrlAsset_image_1 = "http://195.201.125.8/QuantumApp/assets/image_1.jpg"
const val UrlAsset_image_2 = "http://195.201.125.8/QuantumApp/assets/image_2.jpg"
const val UrlAsset_image_3 = "http://195.201.125.8/QuantumApp/assets/image_3.png"
const val UrlAsset_image_4 = "http://195.201.125.8/QuantumApp/assets/image_4.png"
const val UrlAsset_icon_add = "http://195.201.125.8/QuantumApp/assets/icon_add.png"
const val UrlAsset_icon_back = "http://195.201.125.8/QuantumApp/assets/icon_back.png"
const val UrlAsset_icon_briefcase = "http://195.201.125.8/QuantumApp/assets/icon_briefcase.png"
const val UrlAsset_icon_check = "http://195.201.125.8/QuantumApp/assets/icon_check.png"
const val UrlAsset_icon_graph = "http://195.201.125.8/QuantumApp/assets/icon_graph.png"
const val UrlAsset_icon_graph_2 = "http://195.201.125.8/QuantumApp/assets/icon_graph_2.png"
const val UrlAsset_icon_settings = "http://195.201.125.8/QuantumApp/assets/icon_settings.png"
const val UrlLessonFolder = "http://195.201.125.8/QuantumApp/lessons/"