package com.example.quantumapp.data.static

import android.content.Context
import android.graphics.Bitmap
import com.example.quantumapp.util.api.ImageDownloader
import com.example.quantumapp.util.api.QuantumServerClient
import kotlinx.coroutines.*
import kotlin.coroutines.EmptyCoroutineContext

object Assets {

    var back: Bitmap? = null
    var lesson_1: String = ""
    var lesson_2: String = ""
    var lesson_3: String = ""
    var lesson_4: String = ""

    private val loaded : Boolean
    get() = back != null
            && lesson_1.isNotEmpty()
            && lesson_2.isNotEmpty()
            && lesson_3.isNotEmpty()
            && lesson_4.isNotEmpty()

    private val jobs = ArrayList<Job>()

    fun loadAssets(context: Context, onAssetsLoaded : () -> Unit  = {}, onError : (error: Throwable) -> Unit = {}) {
        val scope = CoroutineScope(EmptyCoroutineContext)
        ImageDownloader.urlToBitmap(context, scope, UrlBack,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                back = it
                if(loaded) onAssetsLoaded()
            })
        loadLessons(onAssetsLoaded, onError)
    }

    fun destroy() {
        jobs.onEach { it.cancel() }
    }

    private fun loadLessons(onAssetsLoaded : () -> Unit  = {}, onError : (error: Throwable) -> Unit = {}) {
        jobs.add(loadLesson(1, onSuccess = {
            lesson_1 = it
            if(loaded) onAssetsLoaded()
        }, onError = {
            onError(Throwable("Error"))
        }))
        jobs.add(loadLesson(2, onSuccess = {
            lesson_2 = it
            if(loaded) onAssetsLoaded()
        }, onError = {
            onError(Throwable("Error"))
        }))
        jobs.add(loadLesson(3, onSuccess = {
            lesson_3 = it
            if(loaded) onAssetsLoaded()
        }, onError = {
            onError(Throwable("Error"))
        }))
        jobs.add(loadLesson(4, onSuccess = {
            lesson_4 = it
            if(loaded) onAssetsLoaded()
        }, onError = {
            onError(Throwable("Error"))
        }))
    }

    private fun loadLesson(number: Int, onSuccess: (lesson: String) -> Unit, onError : () -> Unit = {}): Job = CoroutineScope(Dispatchers.IO).async {
        try {
            val lessonRequest = QuantumServerClient.create().getLesson(number)
            if(lessonRequest.isSuccessful) {
                lessonRequest.body()?.let {
                    MainScope().launch { onSuccess(it)  }
                } ?: MainScope().launch { onError() }
            } else  MainScope().launch { onError() }
        } catch (e: Exception) {
            MainScope().launch { onError() }
        }

    }

}