package com.example.quantumapp.data.static

import com.example.quantumapp.data.Investment

object CurrentAppData {
    var url = ""
    var currentLesson = ""
    var currentHeader = ""
    var currentImageUrl = ""

    val investmentsList = ArrayList<Investment>()

    var fontSize = 20.0f
}