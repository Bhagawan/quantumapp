package com.example.quantumapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun CustomTextField(suffix: String = "", modifier: Modifier = Modifier, onTextSet: (String) -> Unit = {}) {
    var text by remember { mutableStateOf("") }
    val focusManager = LocalFocusManager.current
    BasicTextField(
        value = text,
        onValueChange = {
            text = it
            onTextSet(text)
        },
        singleLine = true,
        textStyle = TextStyle(fontSize = 20.sp, color = Color.Black, textAlign = TextAlign.Center, fontWeight = FontWeight.Bold),
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
        keyboardActions = KeyboardActions(onDone = {
            focusManager.clearFocus()
        }),
        decorationBox = { innerTextField ->
            Column(modifier = modifier, verticalArrangement = Arrangement.spacedBy(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                        innerTextField()
                        Text(suffix, color = Color.Black, fontSize = 14.sp, modifier = Modifier.align(
                            Alignment.CenterEnd))
                    }
                }
                HorizontalDivider(modifier = Modifier.fillMaxWidth().background(Color.Black, RoundedCornerShape(10.dp)), thickness = 2.dp, color = Color.Black)
            }
        }
    )
}