package com.example.quantumapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.quantumapp.R
import com.example.quantumapp.data.Investment
import com.example.quantumapp.data.static.CurrentAppData
import com.example.quantumapp.data.static.UrlAsset_icon_back
import com.example.quantumapp.data.static.UrlBack
import com.example.quantumapp.data.static.UrlLogo
import com.example.quantumapp.ui.composables.CustomDatePicker
import com.example.quantumapp.ui.composables.CustomNumberField
import com.example.quantumapp.ui.composables.CustomTextField
import com.example.quantumapp.util.SavedPrefs
import java.util.*

@Composable
fun InvestmentNewScreen(onBack: () -> Unit) {
    var date by remember { mutableStateOf(Date()) }
    var currency by remember { mutableStateOf("") }
    var amount by remember { mutableStateOf(0) }
    val context = LocalContext.current
    val focus = LocalFocusManager.current

    Image(painter = rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    Column(modifier = Modifier
        .fillMaxSize()
        .clickable(MutableInteractionSource(), null) { focus.clearFocus() }
        .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(30.dp)) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Image(
                rememberImagePainter(UrlAsset_icon_back),
                contentDescription = null,
                modifier = Modifier
                    .size(50.dp)
                    .clickable(MutableInteractionSource(), null) { onBack() },
                contentScale = ContentScale.FillBounds
            )
            Box(
                modifier = Modifier
                    .weight(1.0f, true)
                    .height(50.dp),
                contentAlignment = Alignment.CenterEnd
            ) {
                Image(
                    painter = rememberImagePainter(UrlLogo),
                    contentDescription = null,
                    contentScale = ContentScale.FillHeight
                )
            }
        }

        Text(text = stringResource(id = R.string.diary_header ), lineHeight = (CurrentAppData.fontSize * 1.5f).sp, fontSize = (CurrentAppData.fontSize * 2).sp, textAlign = TextAlign.Center, color = Color.White)

        Column(
            modifier = Modifier
                .weight(1.0f, true)
                .background(Color.White, RoundedCornerShape(20.dp))
                .padding(vertical = 10.dp, horizontal = 30.dp),
            horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(text = stringResource(id = R.string.date ), fontSize = CurrentAppData.fontSize.sp, textAlign = TextAlign.Center, color = Color.Black, fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(top = 20.dp))
            CustomDatePicker(modifier = Modifier.fillMaxWidth()) { date = it }
            Text(text = stringResource(id = R.string.currency ), fontSize = CurrentAppData.fontSize.sp, textAlign = TextAlign.Center, color = Color.Black, fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(top = 20.dp))
            CustomTextField(modifier = Modifier.fillMaxWidth()) { currency = it }
            Text(text = stringResource(id = R.string.amount ), fontSize = CurrentAppData.fontSize.sp, textAlign = TextAlign.Center, color = Color.Black, fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(top = 20.dp))
            CustomNumberField(modifier = Modifier.fillMaxWidth()) { amount = it }
        }

        Image(painterResource(id = R.drawable.ic_check_circle),
            contentDescription = null,
            modifier = Modifier
                .padding(bottom = 20.dp)
                .size(100.dp)
                .clickable(MutableInteractionSource(), null)
                {
                    val i = Investment(date, currency, amount)
                    SavedPrefs.addInvestment(context, i)
                    CurrentAppData.investmentsList.add(i)
                    onBack()
                },
            contentScale = ContentScale.FillBounds )

    }
}