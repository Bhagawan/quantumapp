package com.example.quantumapp.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun MenuButton(painter: Painter, text: String = "", fontSize: Float = 20.0f, modifier: Modifier = Modifier, onClick: () -> Unit = {}) {
    Column(modifier = modifier
        .padding(5.dp).wrapContentHeight()
        .clickable(MutableInteractionSource(), null) { onClick() }, horizontalAlignment = Alignment.CenterHorizontally) {
        Image(painter, contentDescription = null, modifier = Modifier.weight(1.0f, true), contentScale = ContentScale.FillHeight)
        Box(modifier = Modifier
            .background(Color.Transparent, RoundedCornerShape(10.dp))
            .padding(10.dp),
            contentAlignment = Alignment.Center ) {
            Text(text = text, fontSize = fontSize.sp, textAlign = TextAlign.Center, color = Color.White, fontWeight = FontWeight.Bold)
        }
    }
}