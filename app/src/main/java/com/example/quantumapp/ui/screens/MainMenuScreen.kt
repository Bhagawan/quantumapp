package com.example.quantumapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.coerceAtMost
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.quantumapp.R
import com.example.quantumapp.data.static.*
import com.example.quantumapp.ui.composables.MenuButton
import com.example.quantumapp.util.navagation.Navigator

@Composable
fun MainMenuScreen() {
    Image(painter = rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        Image(painter = rememberImagePainter(UrlLogo),
            contentDescription = null,
            modifier = Modifier
                .padding(start = 5.dp, top = 5.dp)
                .width(maxWidth / 2)
                .align(Alignment.TopStart),
            contentScale = ContentScale.FillWidth)
        MenuButton(
            rememberImagePainter(UrlAsset_icon_briefcase),
            text = stringResource(id = R.string.lessons),
            fontSize = CurrentAppData.fontSize,
            modifier = Modifier
                .height(maxWidth / 2)
                .wrapContentWidth()
                .align(Alignment.CenterStart)) {
            Navigator.navigateTo(Screens.LESSON_LIST_SCREEN)
        }
        MenuButton(
            rememberImagePainter(UrlAsset_icon_settings),
            text = stringResource(id = R.string.settings),
            fontSize = CurrentAppData.fontSize,
            modifier = Modifier
                .height(maxWidth / 2.8f)
                .wrapContentWidth()
                .offset(x = (maxWidth * 0.6f).coerceAtMost(maxWidth - 200.dp), y = maxHeight / 40)
                .align(Alignment.CenterStart)) {
            Navigator.navigateTo(Screens.SETTINGS_SCREEN)
        }
        MenuButton(
            rememberImagePainter(UrlAsset_icon_graph),
            text = stringResource(id = R.string.visual),
            fontSize = CurrentAppData.fontSize,
            modifier = Modifier
                .height(maxWidth / 3.2f)
                .wrapContentWidth()
                .offset(y =  maxHeight / 40 + maxWidth / 3.3f)
                .align(Alignment.CenterEnd)) {
            Navigator.navigateTo(Screens.GRAPH_SCREEN)
        }
        MenuButton(
            rememberImagePainter(UrlAsset_icon_graph_2),
            text = stringResource(id = R.string.diary),
            fontSize = CurrentAppData.fontSize,
            modifier = Modifier
                .height(maxWidth / 3)
                .wrapContentWidth()
                .offset(x = maxWidth / 4, y = maxHeight / 2.0f + maxWidth / 4)
                .align(Alignment.TopStart)) {
            Navigator.navigateTo(Screens.INVEST_LIST_SCREEN)
        }
    }
}