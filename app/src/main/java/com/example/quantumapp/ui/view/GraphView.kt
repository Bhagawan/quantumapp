package com.example.quantumapp.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.View
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.LinearGradientShader
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.graphics.toArgb
import com.example.quantumapp.data.Investment
import com.example.quantumapp.ui.theme.Blue_light
import com.example.quantumapp.ui.theme.Grey
import com.example.quantumapp.ui.theme.Purple40
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer

class GraphView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var introTimer = 60
    private var topValue = 0.0f
    private var minValue = 0.0f
    private val textSize = 25.0f

    private var indicatorX = -1.0f

    private val investments = ArrayList<Investment>()
    private var graph = Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            createGraph()
        }
    }

    override fun onDraw(canvas: Canvas) {
        drawGraph(canvas)
        drawIndicator(canvas)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_MOVE,
            MotionEvent.ACTION_UP -> {
                indicatorX = event.x
                return true
            }
        }
        return false
    }

    //// Public

    fun setInvestments(list: List<Investment>) {
        investments.run {
            clear()
            addAll(list)
        }
        val v = investments.map { it.amount }
        if(v.isNotEmpty()) {
            topValue = v.max().toFloat()
            minValue = v.min().toFloat()
        }
        createGraph()
    }

    //// Private

    private fun drawGraph(c: Canvas) {
        val p = Paint()
        c.drawColor(Color.BLACK)
        p.strokeWidth = 2.0f
        p.color = Grey.toArgb()
        p.textSize = textSize
        p.isFakeBoldText = false
        p.textAlign = Paint.Align.LEFT
        c.drawText(minValue.toString(),0.0f, mHeight.toFloat(), p)
        c.drawLine(minValue.toString().length * textSize, mHeight - 5.0f, mWidth.toFloat(), mHeight - 5.0f, p)
        p.style = Paint.Style.STROKE
        p.pathEffect = DashPathEffect(floatArrayOf(10.0f, 10.0f), 0.0f)
        val values = listOf(
            String.format("%.2f", minValue),
            String.format("%.2f", (topValue - minValue) * 0.25f),
            String.format("%.2f", (topValue - minValue) * 0.5f),
            String.format("%.2f", (topValue - minValue) * 0.75f),
            String.format("%.2f", topValue)
        )
        for (value in values.withIndex()) {
            if(value.index == 0) {
                p.pathEffect = null
                c.drawText(value.value,0.0f, mHeight.toFloat(), p)
                c.drawLine(value.value.length * textSize, mHeight - 5.0f, mWidth.toFloat(), mHeight - 5.0f, p)
            } else {
                p.pathEffect = DashPathEffect(floatArrayOf(10.0f, 10.0f), 0.0f)
                val y = mHeight.toFloat() * ((values.size -  value.index) / values.size.toFloat())
                val path = Path().apply {
                    moveTo(value.value.length * textSize, y - 5.0f)
                    lineTo(mWidth.toFloat(), y - 5.0f)
                }
                p.pathEffect = null
                c.drawText(value.value,0.0f,  y, p)
                c.drawPath(path, p)
            }
        }
        if(introTimer > 0) {
            val left = String.format("%.2f", minValue).length * textSize
            val g = Bitmap.createBitmap(graph, 0,0, (mWidth - introTimer * (mWidth - left) / 60.0f).toInt(), mHeight)
            c.drawBitmap(g, 0.0f, -introTimer.toFloat(),p)
        } else {
            c.drawBitmap(graph, 0.0f,0.0f,p)
        }
        if(introTimer > 0) introTimer--
    }

    private fun drawIndicator(c: Canvas) {
        val left = String.format("%.2f", minValue).length * textSize
        val oneWidth = (mWidth - left) / investments.size.toFloat()
        if(indicatorX in left..mWidth - 5.0f) {
            var value = minValue
            val width = String.format("%.2f", value).length * textSize
            var circleHeight = minValue
            for(y in 0 until graph.height) {
                if(graph.getPixel(indicatorX.toInt(), y) != Color.TRANSPARENT){
                    circleHeight = y.toFloat()
                }
            }
            for(invest in investments.withIndex()) {
                if(invest.index * oneWidth + left < indicatorX) value = invest.value.amount.toFloat()
            }
            val p = Paint()
            p.strokeWidth = 2.0f
            p.color = Color.WHITE
            p.textSize = textSize
            p.textAlign = Paint.Align.CENTER
            p.isFakeBoldText = true

            c.drawText(String.format("%.2f", value), indicatorX, textSize + 5, p)
            c.drawCircle(indicatorX, circleHeight, 10.0f, p)
            p.color = Color.RED
            c.drawCircle(indicatorX, circleHeight, 7.0f, p)
            p.color = Color.WHITE
            p.style = Paint.Style.STROKE
            c.drawRoundRect(indicatorX - width / 2.0f, 2.0f, indicatorX + width / 2.0f, mHeight - 5.0f, 10.0f, 10.0f, p)
        }
    }

   private fun createGraph() {
       val left = String.format("%.2f", minValue).length * textSize
       val oneWidth = (mWidth - left) / investments.size.toFloat()
       val heightD = (mHeight - 5.0f) / (topValue - minValue)
       val g = Path().apply {
           moveTo(left, mHeight - 5.0f)
           for(invest in investments.withIndex()) {
               lineTo((invest.index + 1) * oneWidth + left, mHeight.toFloat() - 5 - invest.value.amount.toFloat() * heightD)
           }
       }
       val bitmap = Bitmap.createBitmap(mWidth.coerceAtLeast(10), mHeight.coerceAtLeast(10), Bitmap.Config.ARGB_8888)
       val p = Paint()
       p.style = Paint.Style.STROKE
       p.shader = LinearGradientShader(Offset.Zero, Offset(mWidth.toFloat(), mHeight.toFloat()),
           listOf(
               Blue_light,
               Purple40
           ), tileMode = TileMode.Mirror)
       p.strokeWidth = 6.0f
       p.strokeJoin = Paint.Join.ROUND
       p.strokeCap = Paint.Cap.ROUND
       p.isAntiAlias = true
       p.pathEffect = CornerPathEffect(200.0f / investments.size)
       val c = Canvas(bitmap)
       c.drawPath(g, p)
       graph = bitmap
   }
}