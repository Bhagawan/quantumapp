package com.example.quantumapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.quantumapp.R
import com.example.quantumapp.data.static.*
import com.example.quantumapp.util.navagation.Navigator
import com.example.quantumapp.util.shadowOutline

@Composable
fun LessonsListScreen(onBack: () -> Unit) {
    val firstLessonHeader = stringResource(id = R.string.lesson_first)
    val secondLessonHeader = stringResource(id = R.string.lesson_second)
    val thirdLessonHeader = stringResource(id = R.string.lesson_third)
    val forthLessonHeader = stringResource(id = R.string.lesson_forth)
    Image(painter = rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    Column(modifier = Modifier
        .fillMaxSize()
        .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(30.dp)) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Image(rememberImagePainter(UrlAsset_icon_back),
                contentDescription = null,
                modifier = Modifier
                    .size(50.dp)
                    .clickable(MutableInteractionSource(), null) { onBack() },
                contentScale = ContentScale.FillBounds)
            Box(modifier = Modifier
                .weight(1.0f, true)
                .height(50.dp), contentAlignment = Alignment.CenterEnd) {
                Image(painter = rememberImagePainter(UrlLogo),
                    contentDescription = null,
                    contentScale = ContentScale.FillHeight)
            }
        }
        Text(text = stringResource(id = R.string.lessons ), fontSize = (CurrentAppData.fontSize *  2).sp, lineHeight = (CurrentAppData.fontSize * 1.5f).sp, textAlign = TextAlign.Center, color = Color.White)
        Column(modifier = Modifier
            .fillMaxWidth()
            .weight(4.0f, true)
            .padding(vertical = 5.dp)
            .verticalScroll(rememberScrollState()), verticalArrangement = Arrangement.spacedBy(20.dp), horizontalAlignment = Alignment.CenterHorizontally) {
            Text(text = firstLessonHeader,
                fontWeight = FontWeight.Bold,
                fontSize = (CurrentAppData.fontSize *  1.5f).sp,
                textAlign = TextAlign.Center,
                lineHeight = (CurrentAppData.fontSize * 1.5f).sp,
                color = Color.White,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp, vertical = 10.dp)
                    .shadowOutline(cornersRadius = 20.dp)
                    .padding(vertical = 10.dp)
                    .clickable(MutableInteractionSource(), null) {
                        CurrentAppData.currentLesson = Assets.lesson_1
                        CurrentAppData.currentHeader = firstLessonHeader
                        CurrentAppData.currentImageUrl = UrlAsset_image_1
                        Navigator.navigateTo(Screens.LESSON_SCREEN)
                    })
            Text(text = secondLessonHeader,
                fontWeight = FontWeight.Bold,
                fontSize = (CurrentAppData.fontSize *  1.5f).sp,
                textAlign = TextAlign.Center,
                lineHeight = (CurrentAppData.fontSize * 1.5f).sp,
                color = Color.White,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp, vertical = 10.dp)
                    .shadowOutline(cornersRadius = 20.dp)
                    .padding(vertical = 10.dp)
                    .clickable(MutableInteractionSource(), null) {
                        CurrentAppData.currentLesson = Assets.lesson_2
                        CurrentAppData.currentHeader = secondLessonHeader
                        CurrentAppData.currentImageUrl = UrlAsset_image_3
                        Navigator.navigateTo(Screens.LESSON_SCREEN)
                    })
            Text(text = thirdLessonHeader,
                fontWeight = FontWeight.Bold,
                fontSize = (CurrentAppData.fontSize *  1.5f).sp,
                textAlign = TextAlign.Center,
                lineHeight = (CurrentAppData.fontSize * 1.5f).sp,
                color = Color.White,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp, vertical = 10.dp)
                    .shadowOutline(cornersRadius = 20.dp)
                    .padding(vertical = 10.dp)
                    .clickable(MutableInteractionSource(), null) {
                        CurrentAppData.currentLesson = Assets.lesson_3
                        CurrentAppData.currentHeader = thirdLessonHeader
                        CurrentAppData.currentImageUrl = UrlAsset_image_4
                        Navigator.navigateTo(Screens.LESSON_SCREEN)
                    })
            Text(text = forthLessonHeader,
                fontWeight = FontWeight.Bold,
                fontSize = (CurrentAppData.fontSize *  1.5f).sp,
                textAlign = TextAlign.Center,
                lineHeight = (CurrentAppData.fontSize * 1.5f).sp,
                color = Color.White,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp, vertical = 10.dp)
                    .shadowOutline(cornersRadius = 20.dp)
                    .padding(vertical = 10.dp)
                    .clickable(MutableInteractionSource(), null) {
                        CurrentAppData.currentLesson = Assets.lesson_4
                        CurrentAppData.currentHeader = forthLessonHeader
                        CurrentAppData.currentImageUrl = UrlAsset_image_2
                        Navigator.navigateTo(Screens.LESSON_SCREEN)
                    })
        }
    }
}