package com.example.quantumapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.quantumapp.R
import com.example.quantumapp.data.static.CurrentAppData
import com.example.quantumapp.data.static.UrlAsset_icon_back
import com.example.quantumapp.data.static.UrlBack
import com.example.quantumapp.data.static.UrlLogo
import com.example.quantumapp.ui.composables.CustomNumberField
import com.example.quantumapp.ui.theme.Grey
import com.example.quantumapp.ui.theme.Grey_dark
import com.example.quantumapp.util.SavedPrefs

@Composable
fun SettingsScreen(onBack: () -> Unit) {
    val context = LocalContext.current
    val focus = LocalFocusManager.current
    var fontSize by remember { mutableStateOf(CurrentAppData.fontSize) }
    var notification by remember {
        mutableStateOf(SavedPrefs.getNotificationSettings(context))
    }

    Image(painter = rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    Column(modifier = Modifier
        .fillMaxSize()
        .clickable(MutableInteractionSource(), null) { focus.clearFocus() }
        .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(30.dp)) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Image(
                rememberImagePainter(UrlAsset_icon_back),
                contentDescription = null,
                modifier = Modifier
                    .size(50.dp)
                    .clickable(MutableInteractionSource(), null) { onBack() },
                contentScale = ContentScale.FillBounds
            )
            Box(
                modifier = Modifier
                    .weight(1.0f, true)
                    .height(50.dp),
                contentAlignment = Alignment.CenterEnd
            ) {
                Image(
                    painter = rememberImagePainter(UrlLogo),
                    contentDescription = null,
                    contentScale = ContentScale.FillHeight
                )
            }
        }

        Text(text = stringResource(id = R.string.settings ), fontSize = (fontSize * 2).sp, textAlign = TextAlign.Center, color = Color.White)

        Spacer(modifier = Modifier.weight(1.0f, true))
        Row(modifier = Modifier.wrapContentWidth(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
            Text(text = stringResource(id = R.string.settings_notifications ), fontSize = fontSize.sp, textAlign = TextAlign.Center, color = Color.White)
            Switch(checked = notification, onCheckedChange = {
                notification = notification.not()
                SavedPrefs.saveNotificationSettings(context, notification)
            }, colors = SwitchDefaults.colors(
                uncheckedTrackColor = Grey_dark,
                checkedTrackColor = Grey,
                uncheckedThumbColor = Color.White,
                checkedThumbColor = Color.White
            ))
        }
        Row(modifier = Modifier.wrapContentWidth(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
            Text(text = stringResource(id = R.string.settings_font ), fontSize = fontSize.sp, textAlign = TextAlign.Center, color = Color.White)
            CustomNumberField(modifier = Modifier.width(100.dp).background(Color.White, RoundedCornerShape(10.dp)),
            allowedInterval = 10..40,
            initValue = fontSize.toInt()) {
                fontSize = it.toFloat()
                CurrentAppData.fontSize = fontSize
                SavedPrefs.saveFontSize(context, it.toFloat())
            }
        }
        Spacer(modifier = Modifier.weight(1.0f, true))
    }
}