package com.example.quantumapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.quantumapp.R
import com.example.quantumapp.data.static.UrlBack

@Composable
fun NetworkErrorScreen(onPress: () -> Unit) {
    Image(painter = rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    Box(modifier = Modifier
        .fillMaxSize(), contentAlignment = Alignment.Center) {
        Column(verticalArrangement = Arrangement.spacedBy(5.dp), horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier.fillMaxWidth(0.8f)) {
            Text(text = stringResource(id = R.string.error_network), textAlign = TextAlign.Center, fontWeight = FontWeight.Bold, color = Color.White, fontSize = 20.sp)
            Image(
                painter = painterResource(id = R.drawable.ic_restart),
                contentDescription = stringResource(
                    id = R.string.desc_reload),
                contentScale = ContentScale.FillWidth,
                modifier = Modifier
                    .fillMaxWidth(0.3f)
                    .clickable { onPress() })
        }

    }
}