package com.example.quantumapp.ui.composables

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.quantumapp.data.Investment

@Composable
fun InvestmentItem(investment: Investment, fontSize: Float = 20.0f, modifier: Modifier = Modifier) {
    val date = android.text.format.DateFormat.format("dd.MM.yyyy", investment.date).toString()
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically) {
        Text(text = date,
            fontSize = fontSize.sp,
            textAlign = TextAlign.Start,
            color = Color.White,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.weight(1.0f, true))
        Text(text = investment.amount.toString() + "$",
            fontSize = fontSize.sp,
            textAlign = TextAlign.Center,
            color = Color.White,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(horizontal = 10.dp))
    }
}