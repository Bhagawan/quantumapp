package com.example.quantumapp.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    WEB_VIEW("web_view"),
    NETWORK_ERROR_SCREEN("network_error"),
    LESSON_LIST_SCREEN("lesson_list"),
    LESSON_SCREEN("lesson"),
    INVEST_LIST_SCREEN("invest_list"),
    INVEST_NEW("invest_new"),
    GRAPH_SCREEN("graph"),
    SETTINGS_SCREEN("settings"),
    MENU_SCREEN("menu")
}