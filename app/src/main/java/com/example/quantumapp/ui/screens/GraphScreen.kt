package com.example.quantumapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import coil.compose.rememberImagePainter
import com.example.quantumapp.R
import com.example.quantumapp.data.static.CurrentAppData
import com.example.quantumapp.data.static.UrlAsset_icon_back
import com.example.quantumapp.data.static.UrlBack
import com.example.quantumapp.data.static.UrlLogo
import com.example.quantumapp.ui.view.GraphView

@Composable
fun GraphScreen(onBack: () -> Unit) {

    Image(painter = rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    Column(modifier = Modifier
        .fillMaxSize()
        .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(30.dp)) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Image(
                rememberImagePainter(UrlAsset_icon_back),
                contentDescription = null,
                modifier = Modifier
                    .size(50.dp)
                    .clickable(MutableInteractionSource(), null) { onBack() },
                contentScale = ContentScale.FillBounds
            )
            Box(
                modifier = Modifier
                    .weight(1.0f, true)
                    .height(50.dp),
                contentAlignment = Alignment.CenterEnd
            ) {
                Image(
                    painter = rememberImagePainter(UrlLogo),
                    contentDescription = null,
                    contentScale = ContentScale.FillHeight
                )
            }
        }

        Text(text = stringResource(id = R.string.visual ), fontSize = (CurrentAppData.fontSize * 2).sp, textAlign = TextAlign.Start, color = Color.White,
            modifier = Modifier.align(Alignment.Start))

        BoxWithConstraints(modifier = Modifier.fillMaxWidth().weight(1.0f, true)) {
            AndroidView(factory = { GraphView(it) },
                modifier = Modifier
                    .fillMaxWidth(0.9f)
                    .height(maxHeight / 3)
                    .clipToBounds(),
                update = {
                    it.setInvestments(CurrentAppData.investmentsList)
                })
        }
    }
}