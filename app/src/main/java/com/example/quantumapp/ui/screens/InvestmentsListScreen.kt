package com.example.quantumapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.quantumapp.R
import com.example.quantumapp.data.static.CurrentAppData
import com.example.quantumapp.data.static.UrlAsset_icon_back
import com.example.quantumapp.data.static.UrlBack
import com.example.quantumapp.data.static.UrlLogo
import com.example.quantumapp.ui.composables.InvestmentItem

@Composable
fun InvestmentsListScreen(onBack: () -> Unit, onNew: () -> Unit) {
    val investments = CurrentAppData.investmentsList
    val investmentsScrollState = rememberLazyListState()

    Image(painter = rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    Column(modifier = Modifier
        .fillMaxSize()
        .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(30.dp)) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Image(
                rememberImagePainter(UrlAsset_icon_back),
                contentDescription = null,
                modifier = Modifier
                    .size(50.dp)
                    .clickable(MutableInteractionSource(), null) { onBack() },
                contentScale = ContentScale.FillBounds
            )
            Box(
                modifier = Modifier
                    .weight(1.0f, true)
                    .height(50.dp),
                contentAlignment = Alignment.CenterEnd
            ) {
                Image(
                    painter = rememberImagePainter(UrlLogo),
                    contentDescription = null,
                    contentScale = ContentScale.FillHeight
                )
            }
        }
        Text(text = stringResource(id = R.string.diary_header ), fontSize = (CurrentAppData.fontSize * 2).sp, lineHeight = (CurrentAppData.fontSize * 1.5f).sp, textAlign = TextAlign.Center, color = Color.White)

        LazyColumn(state = investmentsScrollState, modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true), verticalArrangement = Arrangement.spacedBy(10.dp), horizontalAlignment = Alignment.CenterHorizontally) {
            items(investments) { investment ->
                InvestmentItem(investment = investment)
            }
        }

        Image(painterResource(id = R.drawable.ic_baseline_add_circle_outline_24),
            contentDescription = null,
            modifier = Modifier
                .padding(bottom = 20.dp)
                .size(100.dp)
                .clickable(MutableInteractionSource(), null) { onNew() },
            contentScale = ContentScale.FillBounds )

    }
}