package com.example.quantumapp.ui.composables

import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import java.util.*

@Composable
fun CustomDatePicker(modifier: Modifier = Modifier, onDateSet: (Date) -> Unit) {
    val mContext = LocalContext.current
    val mCalendar = Calendar.getInstance()

    val year: Int = mCalendar.get(Calendar.YEAR)
    val month: Int = mCalendar.get(Calendar.MONTH)
    val day: Int = mCalendar.get(Calendar.DAY_OF_MONTH)

    mCalendar.time = Date()

    var date by remember { mutableStateOf(Date()) }

    val datePickerDialog = DatePickerDialog(
        mContext,
        { _: DatePicker, mYear: Int, mMonth: Int, mDayOfMonth: Int ->
            val c = Calendar.getInstance()
            c.set(mYear, mMonth, mDayOfMonth)
            date = c.time
            onDateSet(date)
        }, year, month, day
    )

    val dateString = android.text.format.DateFormat.format("dd.MM.yyyy", date).toString()
    Column(modifier = modifier.clickable(MutableInteractionSource(), null) { datePickerDialog.show() }, horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
        Text(text = dateString, fontSize = 20.sp, textAlign = TextAlign.Center, color = Color.Black, fontWeight = FontWeight.Bold)
        HorizontalDivider(modifier = Modifier.fillMaxWidth().background(Color.Black, RoundedCornerShape(10.dp)), thickness = 2.dp, color = Color.Black)
    }


}