package com.example.quantumapp.ui.screens

import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.quantumapp.data.static.CurrentAppData
import com.example.quantumapp.data.static.UrlAsset_icon_back
import com.example.quantumapp.data.static.UrlBack
import com.example.quantumapp.data.static.UrlLogo
import com.example.quantumapp.util.shadowOutline

@Composable
fun LessonScreen(header: String, lesson: String, imageUrl: String, onBack: () -> Unit) {
    val scrollState = rememberScrollState()
    Image(painter = rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    Column(modifier = Modifier
        .fillMaxSize()
        .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Image(
                rememberImagePainter(UrlAsset_icon_back),
                contentDescription = null,
                modifier = Modifier
                    .size(50.dp)
                    .background(Color.Transparent, CircleShape)
                    .clickable(MutableInteractionSource(), null) { onBack() },
                contentScale = ContentScale.FillBounds)
            Box(modifier = Modifier
                .weight(1.0f, true)
                .height(50.dp), contentAlignment = Alignment.CenterEnd) {
                Image(painter = rememberImagePainter(UrlLogo),
                    contentDescription = null,
                    contentScale = ContentScale.FillHeight)
            }
        }
        Column(modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true)
            .verticalScroll(scrollState),
            verticalArrangement = Arrangement.spacedBy(20.dp),
            horizontalAlignment = Alignment.CenterHorizontally) {
            Text(header, fontSize = (CurrentAppData.fontSize * 2).sp, lineHeight = (CurrentAppData.fontSize * 1.5f).sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, color = Color.White, modifier = Modifier
                .padding(top = 20.dp)
                .shadowOutline()
                .padding(5.dp))
            Image(painter = rememberImagePainter(imageUrl), contentDescription = null, modifier = Modifier
                .height(200.dp)
                .shadowOutline(cornersRadius = 1.dp), contentScale = ContentScale.FillHeight)
            Text(lesson, fontSize = (CurrentAppData.fontSize * 0.75f).sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Start, color = Color.White, modifier = Modifier.fillMaxWidth())
        }
    }
}