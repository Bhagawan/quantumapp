package com.example.quantumapp.util

import android.graphics.BlurMaskFilter
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.PaintingStyle
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

fun Modifier.shadowOutline(
    color: Color = Color.White,
    cornersRadius: Dp = 10.dp,
    blur: Dp = 9.5f.dp
) = drawWithContent {
    val rect = Rect(Offset(-blur.toPx() * 2, -blur.toPx() * 2), Offset(size.width + blur.toPx() * 2, size.height + blur.toPx() * 2))
    val paint = Paint()

    drawIntoCanvas {

        paint.color = color
        paint.isAntiAlias = true
        paint.style = PaintingStyle.Fill
        paint.strokeWidth = 80.0f
        it.saveLayer(rect, paint)
        it.drawRoundRect(
            left = rect.left,
            top = rect.top,
            right = rect.right,
            bottom = rect.bottom,
            cornersRadius.toPx(),
            cornersRadius.toPx(),
            paint
        )
        paint.style = PaintingStyle.Stroke
        val frameworkPaint = paint.asFrameworkPaint()
        frameworkPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OUT)
        if (blur.toPx() > 0) {
            frameworkPaint.maskFilter = BlurMaskFilter(blur.toPx(), BlurMaskFilter.Blur.NORMAL)
        }

        paint.color = Color.Black
        it.drawRoundRect(
            left = rect.left,
            top = rect.top,
            right = rect.right ,
            bottom = rect.bottom ,
            cornersRadius.toPx(),
            cornersRadius.toPx(),
            paint
        )

        paint.color = Color.Transparent
        paint.style = PaintingStyle.Fill
        frameworkPaint.maskFilter = BlurMaskFilter(0.1f, BlurMaskFilter.Blur.SOLID)
        frameworkPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        it.drawRoundRect(
            left = 0.0f,
            top = 0.0f,
            right = size.width ,
            bottom = size.height ,
            cornersRadius.toPx(),
            cornersRadius.toPx(),
            paint
        )
        it.restore()
        frameworkPaint.maskFilter = null
        frameworkPaint.xfermode = null
    }
    drawContent()
}