package com.example.quantumapp.util.navagation

import android.webkit.WebView
import androidx.activity.compose.BackHandler
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.quantumapp.data.static.CurrentAppData
import com.example.quantumapp.ui.screens.*
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: WebView, fixateScreen:  () -> Unit, reloadAssets: () -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            BackHandler(true) { exitProcess(0) }
            SplashScreen()
        }
        composable(Screens.NETWORK_ERROR_SCREEN.label) {
            BackHandler(true) { exitProcess(0) }
            NetworkErrorScreen(reloadAssets)
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.canGoBack()) webView.goBack()
                else exitProcess(0)
            }
            WebViewScreen(webView, remember { CurrentAppData.url })
        }
        composable(Screens.MENU_SCREEN.label,
            exitTransition = {
                fadeOut(tween(durationMillis = 1, delayMillis = 2000))
            }) {
            BackHandler(true) {}
            MainMenuScreen()
        }
        composable(Screens.LESSON_LIST_SCREEN.label,
            enterTransition = {
                if(navController.previousBackStackEntry?.destination?.route == Screens.LESSON_SCREEN.label) {
                    null
                }
                else slideInHorizontally(initialOffsetX = { 2 * it }, animationSpec = spring( stiffness = Spring.StiffnessLow))
            },
            exitTransition = {
                fadeOut(tween(durationMillis = 1, delayMillis = 2000))
            }) {
            BackHandler(true) { Navigator.navigateTo(Screens.MENU_SCREEN) }
            LessonsListScreen { Navigator.navigateTo(Screens.MENU_SCREEN) }
        }
        composable(Screens.LESSON_SCREEN.label,
            enterTransition = {
                slideInVertically(initialOffsetY = { 2 * it }, animationSpec = spring( stiffness = Spring.StiffnessLow))
            }) {
            BackHandler(true) { Navigator.navigateTo(Screens.LESSON_LIST_SCREEN) }
            LessonScreen(CurrentAppData.currentHeader, CurrentAppData.currentLesson, CurrentAppData.currentImageUrl) { Navigator.navigateTo(Screens.LESSON_LIST_SCREEN) }
        }
        composable(Screens.INVEST_LIST_SCREEN.label,
            enterTransition = {
                if(navController.previousBackStackEntry?.destination?.route == Screens.INVEST_NEW.label) {
                    null
                }
                else slideInHorizontally(initialOffsetX = { 2 * it }, animationSpec = spring( stiffness = Spring.StiffnessLow))
            },
            exitTransition = {
                fadeOut(tween(durationMillis = 1, delayMillis = 2000))
            }) {
            BackHandler(true) { Navigator.navigateTo(Screens.MENU_SCREEN) }
            InvestmentsListScreen( onBack = { Navigator.navigateTo(Screens.MENU_SCREEN) }, onNew = { Navigator.navigateTo(Screens.INVEST_NEW) })
        }
        composable(Screens.INVEST_NEW.label,
            enterTransition = {
                slideInVertically(initialOffsetY = { 2 * it }, animationSpec = spring( stiffness = Spring.StiffnessLow))
            },
            exitTransition = {
                slideOutVertically (targetOffsetY = { 2 * it }, animationSpec = spring( stiffness = Spring.StiffnessLow))
            }) {
            BackHandler(true) { Navigator.navigateTo(Screens.INVEST_LIST_SCREEN) }
            InvestmentNewScreen { Navigator.navigateTo(Screens.INVEST_LIST_SCREEN) }
        }
        composable(Screens.GRAPH_SCREEN.label,
            enterTransition = {
                slideInHorizontally(initialOffsetX = { 2 * it }, animationSpec = spring( stiffness = Spring.StiffnessLow))
            },
            exitTransition = {
                fadeOut(tween(durationMillis = 1, delayMillis = 2000))
            }) {
            BackHandler(true) { Navigator.navigateTo(Screens.MENU_SCREEN) }
            GraphScreen { Navigator.navigateTo(Screens.MENU_SCREEN) }
        }
        composable(Screens.SETTINGS_SCREEN.label,
            enterTransition = {
                slideInHorizontally(initialOffsetX = { 2 * it }, animationSpec = spring( stiffness = Spring.StiffnessLow))
            },
            exitTransition = {
                fadeOut(tween(durationMillis = 1, delayMillis = 2000))
            }) {
            BackHandler(true) { Navigator.navigateTo(Screens.MENU_SCREEN) }
            SettingsScreen { Navigator.navigateTo(Screens.MENU_SCREEN) }
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    if(currentScreen != Screens.WEB_VIEW) fixateScreen()
    navController.navigate(currentScreen.label) {
        launchSingleTop = true
    }
    navController.enableOnBackPressed(true)
}