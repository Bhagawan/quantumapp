package com.example.quantumapp.util.api

import com.example.quantumapp.data.QuantumSplashResponse
import com.example.quantumapp.data.static.UrlLessonFolder
import com.example.quantumapp.data.static.UrlSplash
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

interface QuantumServerClient {

    @FormUrlEncoded
    @POST(UrlSplash)
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<QuantumSplashResponse>

    @GET(UrlLessonFolder + "lesson_{number}.txt")
    suspend fun getLesson(@Path("number") number: Int): Response<String>


    companion object {
        fun create() : QuantumServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(QuantumServerClient::class.java)
        }
    }
}