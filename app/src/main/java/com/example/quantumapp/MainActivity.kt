package com.example.quantumapp

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.FrameLayout
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.compose.rememberNavController
import com.example.quantumapp.data.static.Assets
import com.example.quantumapp.data.static.CurrentAppData
import com.example.quantumapp.ui.screens.Screens
import com.example.quantumapp.ui.theme.QuantumAppTheme
import com.example.quantumapp.util.SavedPrefs
import com.example.quantumapp.util.navagation.NavigationComponent
import com.example.quantumapp.util.navagation.Navigator

class MainActivity : ComponentActivity() {
    private lateinit var filePathCallback: ValueCallback<Array<Uri>>
    private lateinit var viewModel :QuantumMainViewModel

    var mLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if(it.resultCode == RESULT_OK) {
            it.data?.let { intent ->
                intent.data?.let { uri ->
                    filePathCallback.onReceiveValue(arrayOf(uri))
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CurrentAppData.investmentsList.clear()
        CurrentAppData.investmentsList.addAll(SavedPrefs.getInvestmentsList(this))
        CurrentAppData.fontSize = SavedPrefs.getFontSize(this)
        viewModel = ViewModelProvider(this)[QuantumMainViewModel::class.java]
        if(savedInstanceState == null) viewModel.init((getSystemService(TELEPHONY_SERVICE) as TelephonyManager).simCountryIso, SavedPrefs.getId(this))
        Assets.loadAssets(this, onAssetsLoaded = viewModel::assetsLoaded, onError = { Navigator.navigateTo(Screens.NETWORK_ERROR_SCREEN) })
        setContent {
            val webView = remember { createWebView() }
            val navController = rememberNavController()
            QuantumAppTheme {
                Surface(
                    modifier = Modifier.fillMaxSize().background(Color.Black),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavigationComponent(navController, webView, ::fixScreen , ::reloadAssets)
                }
            }
        }
    }

    override fun onDestroy() {
        Assets.destroy()
        super.onDestroy()
    }

    private fun reloadAssets() {
        Navigator.navigateTo(Screens.SPLASH_SCREEN)
        Assets.loadAssets(this, onError = {
            Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
        }, onAssetsLoaded = viewModel::assetsLoaded)
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private  fun fixScreen() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun createWebView() : WebView {
        return WebView(this).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            CookieManager.getInstance().setAcceptThirdPartyCookies(this, true)
            webViewClient = WebViewClient()
            webChromeClient = object: WebChromeClient() {
                private var h = 0
                private var uiVisibility = 0
                private var d: View? = null
                private var a: CustomViewCallback? = null
                override fun getDefaultVideoPoster(): Bitmap? = if(d == null) null else BitmapFactory.decodeResource(resources, R.drawable.ic_baseline_video)
                override fun onHideCustomView() {
                    (window.decorView as FrameLayout).removeView(d)
                    d = null
                    window.decorView.systemUiVisibility = uiVisibility
                    requestedOrientation = h
                    a!!.onCustomViewHidden()
                    a = null
                }

                override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                    if(d != null) {
                        onHideCustomView()
                        return
                    }
                    d = view
                    uiVisibility = window.decorView.systemUiVisibility
                    h = requestedOrientation
                    a = callback
                    (window.decorView as FrameLayout).addView(d, FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
                    WindowInsetsControllerCompat(window, window.decorView).let { controller ->
                        controller.hide(WindowInsetsCompat.Type.systemBars())
                        controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    }
                }

                override fun onShowFileChooser(view: WebView, filePath: ValueCallback<Array<Uri>>, fileChooserParams: FileChooserParams): Boolean {
                    filePathCallback = filePath
                    val i = Intent(Intent.ACTION_GET_CONTENT)
                    i.addCategory(Intent.CATEGORY_OPENABLE)
                    i.type = "image/*"
                    i.putExtra(Intent.EXTRA_TITLE, "File Chooser")
                    mLauncher.launch(i)
                    return true
                }
            }
            settings.domStorageEnabled = true
            settings.allowContentAccess = true
            settings.allowFileAccess = true
            settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            settings.loadWithOverviewMode = true
            settings.useWideViewPort = true
            settings.javaScriptEnabled = true
            settings.userAgentString = settings.userAgentString.replace("; wv", "")
        }
    }
}