package com.example.quantumapp

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.quantumapp.data.static.CurrentAppData
import com.example.quantumapp.ui.screens.Screens
import com.example.quantumapp.util.api.QuantumServerClient
import com.example.quantumapp.util.navagation.Navigator
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class QuantumMainViewModel: ViewModel() {
    private var request: Job? = null
    private val server = QuantumServerClient.create()
    private var phpApp = false
    private var assetsLoaded = false

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = server.getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> switchToApp()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                switchToApp()
                            }
                            else -> viewModelScope.launch {
                                CurrentAppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else switchToApp()
                } else switchToApp()
            }
        } catch (e: Exception) {
            switchToApp()
        }
    }

    fun assetsLoaded() {
        assetsLoaded = true
        toApp()
    }

    private fun toApp() {
        if(phpApp && assetsLoaded)Navigator.navigateTo(Screens.MENU_SCREEN)
    }

    private fun switchToApp() {
        phpApp = true
        toApp()
    }
}